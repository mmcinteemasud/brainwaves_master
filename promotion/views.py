from django.contrib.admin.views.decorators import staff_member_required
from django.shortcuts import render
import datetime
import pickle
import csv
import os.path

# Create your views here.
#@staff_member_required
def Promotion(request):
	return render(request, 'promotion/promotion-ad.html',)

def Success(request):
	promotion_file = 'storage/30PrctOff-2018-12-08.txt'
	date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
	firstname = request.POST.get("firstname").strip()
	lastname = request.POST.get("lastname").strip()
	email = request.POST.get("email").strip()
	promotion = "30PrctOff-2018-12-08"
	#promotion_code=['']
	message=['']
	#This function will distribute a new code to each request when called
	def pop_code():
		with open('storage/data.txt', 'rb') as code_list:
			now_data = pickle.load(code_list)
		if not now_data:
			global promotion_code
			message[0]="Sorry, all of the promotion coupons have been distributed."
			promotion_code = 'All gone'
		else:
			global promotion_code
			promotion_code=now_data.pop(-1)
			with open('storage/data.txt', 'wb') as f:
				pickle.dump(now_data, f)
	#serialize the text file with promotion codes to data.txt so python list methods can be used
	if os.path.isfile('storage/data.txt'):
		pass
	else:
		with open(promotion_file, 'r') as f:
			content = f.readlines()
			# you may also want to remove whitespace characters like `\n` at the end of each line
		codes = [x.strip() for x in content]
		with open('storage/data.txt', 'wb') as f:
			pickle.dump(codes, f)

	#Write customer info to csv or check if already exists
	if os.path.isfile('storage/customers.csv'):
		with open('storage/customers.csv', 'rt') as fr:
			reader = csv.DictReader(fr)

			for row in reader:
				if row['email'] == email:
					if row['promotion'] == promotion:
						global promotion_code
						message[0]= "Sorry, a code has been given to this email already."
						promotion_code = ''
			if message[0] == '':
				pop_code()
				with open('storage/customers.csv', 'a') as csvfile:
					fieldnames = ['date', 'firstname', 'lastname', 'email', 'promotion', 'promotion_code']
					writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
					writer.writerow({'date': date, 'firstname': firstname, 'lastname': lastname, 'email': email, 'promotion': promotion, 'promotion_code': promotion_code})
	else:
		pop_code()
		with open('storage/customers.csv', 'w') as csvfile:
			fieldnames = ['date', 'firstname', 'lastname', 'email', 'promotion', 'promotion_code']
			writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
			writer.writeheader()
			writer.writerow({'date': date, 'firstname': firstname, 'lastname': lastname, 'email': email, 'promotion': promotion, 'promotion_code': promotion_code})
	
	return render(request, 'promotion/success-page.html',{'code':promotion_code, 'message': message[0]})
