 $(function() { 
  $('input[type="checkbox"]').bind('click',function() {
    $('input[type="checkbox"]').not(this).prop("checked", false);
  });
});

$(document).on("click", ".searchbutton", function(e){
		console.log("hello");
		var query_list = [];
		var search_term = $(document).find('.searchfield').val();
		var filename = $(document).find('.filename').text();
		if (search_term == ''){
			console.log('blank search term');
			return
		}
		else{
		var this_query = {'search_term': search_term, 'filename':filename};
		query_list.push(this_query);
		search = JSON.stringify(query_list);
		console.log(search);
        $.ajax({
            url: '/contacts/search/',
			type: 'POST',
			data: search,
			dataType: 'json',
            success: function(data){
				 var table =  data['data'];
				 $('.responsive-table').html(table);
				 //console.log(response['status']);
            }
        });
 }
});

$(document).ready(function() {
$('#searchform').keypress(function (e) {                                       
       if (e.which == 13) {
       	console.log('enter pressed')
       	$('.searchbutton').click();
            return false;

            //do something   
       }
});
});
//Responsive pagination of table
function ajax_get_update()
    {	
       $.get(url, function(results){
          //get the parts of the result you want to update. Just select the needed parts of the response
          
          var table =  results;
          //var page = $(".list-pagination", results);

          //update the ajax_table_result with the return value
          $('.responsive-table').html(table);
          /*$('.list-pagination').html(page);*/
        }, "html");
    }
$(document).on('click', '.paginate-previous', function(e) {
  console.log("Previous button Debuggin...");
    e.preventDefault();
    url = ($( '.paginate-previous' )[0].href);
    ajax_get_update();
});
$(document).on('click', '.paginate-first', function(e) {
  console.log("First button Debuggin...");
    e.preventDefault();
    url = ($( '.paginate-first' )[0].href);
    ajax_get_update();
});
$(document).on('click', '.next', function(e) {
  console.log("Next button Debuggin...");
    e.preventDefault();
    url = ($( '.next' )[0].href);
    ajax_get_update();
});
$(document).on('click', '.last', function(e) {
  console.log("Next button Debuggin...");
    e.preventDefault();
    url = ($( '.last' )[0].href);
    ajax_get_update();
});
// End responsive pagination of table

//send email

$(document).on("click", "#send-email", function(e){
    e.preventDefault();
    console.log("hello");
    var email_data = [];
    var code_given_list = [''];
    $.each($("input[type='checkbox']:checked"), function(){
      code_given_list.push($(this).val());
    })
    var code_given = code_given_list[code_given_list.length -1];
    console.log(code_given);
    var send_to = $(document).find('#sendto-dropdown').val();
    var from_date = $(document).find('#date-from-picker').val();
    var to_date =  $(document).find('#date-to-picker').val();
    var fromdate = new Date(from_date);
    var from_date = (fromdate.getFullYear() + '-' + (fromdate.getMonth() + 1) + '-' + fromdate.getDate());
    var todate = new Date(to_date);
    var to_date = (todate.getFullYear() + '-' + (todate.getMonth() + 1) + '-' + todate.getDate());
    var promotion = $(document).find('#promotion-dropdown').val();
    var name_list = $(document).find('#name-dropdown').val();
    var email_list = $(document).find('#email-dropdown').val();
    var promotion_codes = $(document).find('#promo-code-input').val();
    var subject = $(document).find('#subject-input').val();
    var message = $(document).find('#message-input').val();
    console.log(message);
    var this_filter = {'send_to': send_to, 'from_date':from_date, 'to_date': to_date, 'promotion': promotion, 'code_given':code_given, 'name_list': name_list, 'email_list': email_list, 'promotion_codes': promotion_codes, 'subject':subject, 
    'message': message};
    email_data.push(this_filter);
    email_filters = JSON.stringify(email_data);
    console.log(email_filters);
        $.ajax({
            url: '/contacts/mail-confirm/',
      type: 'POST',
      data: email_filters,
      contentType: 'application/json; charset=utf-8',
      dataType: 'json',
            success: function(data){
          $('.messaging-form').height(0);
          var html =  data['data'];
          var message = data['message'];
         $('.mail-confirmation').html(html);
         $('.message-template').html(message);
         $('.mail-confirmation').show();
         $('.messaging-container').height('auto');
         $('html, body').animate({ scrollTop: 0 }, 0);
         //window.location.href = response;
            }
        });
 
});

$(document).on("click", "#edit-email-button", function(e){
    e.preventDefault();
    $('.mail-confirmation').hide();
    $('.messaging-form').height('auto');
    $('html, body').animate({ scrollTop: 0 }, 0);

    });

$(document).on("click", "#send-email-button", function(e){
    e.preventDefault();
    console.log("hello");
    var filename_data = [];
    var filename = $(document).find('#message-file').text();
    var this_file = {'filename': filename};
    filename_data.push(this_file);
    email_file = JSON.stringify(filename_data);
    console.log(email_file);
        $.ajax({
            url: '/contacts/send-mail/',
      type: 'POST',
      data: email_file,
      contentType: 'application/json; charset=utf-8',
      dataType: 'json',
            success: function(response){
          var message =  response['data'];
         window.location.href = '/contacts/mail-sent/';
            }
        });
 
});

$(document).on("click", "#add-contact-button", function(e){
    e.preventDefault();
    console.log("hello");
    var contact_data = [];
    var firstname = $(document).find('#firstname-input').val();
    var lastname = $(document).find('#lastname-input').val();
    var email =  $(document).find('#email-input').val();
    var promotion = $(document).find('#promotion-input').val();
    var promotion_code = $(document).find('#promotion-code-input').val();
    var this_contact = {'firstname': firstname, 'lastname': lastname, 'email': email, 'promotion': promotion, 'promotion_code': promotion_code};
    contact_data.push(this_contact);
    contact = JSON.stringify(contact_data);
    console.log(contact);
        $.ajax({
            url: '/contacts/add-contacts/',
      type: 'POST',
      data: contact,
      contentType: 'application/json; charset=utf-8',
      dataType: 'json',
            success: function(data){
              $('.add-contacts-container').find("input[type=text]").val("");
              var html =  data['data'];
              $('.save-confirmation').html(html);
             }
        });
 
});