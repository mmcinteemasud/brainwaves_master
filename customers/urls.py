from django.conf.urls import url,patterns
from customers import views


urlpatterns = [
	url(r'^$', views.Contacts, name='contacts'),
	url(r'^messaging/$', views.Messaging, name='messaging'),
	url(r'^download/$', views.Download, name='download'),
	url(r'^search/$', views.Search, name='search'),
	url(r'^mail-confirm/$', views.Mail_Confirm, name='mail-confirm'),
	url(r'^send-mail/$', views.Send_Mail, name='send-mail'),
	url(r'^mail-sent/$', views.Mail_Sent, name='mail-sent'),
	url(r'^files/$', views.Files, name='files'),
	url(r'^upload/$', views.Upload, name='upload'),
	url(r'^add-contacts/$', views.Add_Contacts, name='add-contacts'),

	]