from django.template.loader import render_to_string
from django.utils.html import strip_tags
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

def send_email(name, email, subject, message):
	sender = 'BrainWaves Toys <admin@brainwavestoystore.com>'
	recipient = email
	# Create message
	htmlcontent = render_to_string(message, {'name': name})
	textcontent = strip_tags(htmlcontent)
	msg = MIMEMultipart('alternative')
	msg['Subject'] = subject
	msg['From'] = sender
	msg['To'] = recipient
	mssgplain = MIMEText(textcontent, 'plain')
	mssghtml = MIMEText(htmlcontent, 'html')
	msg.attach(mssgplain)
	msg.attach(mssghtml)

	# Create server object with SSL option
	server = smtplib.SMTP_SSL('smtp.zoho.com', 465)

	# Perform operations via server
	server.login('admin@brainwavestoystore.com', 'Amarachi1')
	server.sendmail(sender, [recipient], msg.as_string())
	server.quit()


