from __future__ import absolute_import

from celery import task
from .utils import send_email

@task
def SendEmail(name, email, subject, message):
	send_email(name, email, subject, message)
	return 'Email Sent'
