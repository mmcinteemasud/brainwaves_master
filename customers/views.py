from django.contrib.admin.views.decorators import staff_member_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render
from django.conf import settings
from django.core.files.storage import FileSystemStorage
import datetime
import csv
import os.path
import json
from django.http import JsonResponse
from django.http import HttpResponse
from django.template.loader import get_template
from django.template.loader import render_to_string
from django.template import Context
import string
import pickle
from .tasks import SendEmail

# Create your views here.

@staff_member_required
def Contacts(request):
	contact_list = []
	current_filename = 'storage/customers.csv'
	if os.path.isfile(current_filename):
		with open(current_filename, 'r') as f:
			reader = csv.DictReader(f)
			for row in reader:
				contact_list.append(row)
			#print(len(contact_list))
		contact_list.reverse()
		paginator = Paginator(contact_list, 10)
		page = request.GET.get('page')
		try:
			contacts = paginator.page(page)
		except PageNotAnInteger:
			contacts = paginator.page(1)
		except EmptyPage:
			contacts = paginator.page(paginator.num_pages)
		
		if request.is_ajax():
			paginator = Paginator(contact_list, 10)
			page = request.GET.get('page')
			try:
				contacts = paginator.page(page)
			except PageNotAnInteger:
				contacts = paginator.page(1)
			except EmptyPage:
				contacts = paginator.page(paginator.num_pages)
			return render(request, 'customers/responsive-table.html', {"contacts": contacts, 'contacts_total':len(contact_list), "filename": current_filename})
	
		return render(request, 'customers/customer_list.html', {"contacts": contacts, 'contacts_total':len(contact_list), "filename": current_filename})
	else:
		return render(request, 'customers/customer_list.html', {"contacts": '', 'contacts_total':0, "filename": ''})
@staff_member_required
def Messaging(request):
	names_list = []
	emails_list = []
	promotions_list = []
	current_filename = 'storage/customers.csv'
	with open(current_filename, 'r') as f:
		reader = csv.DictReader(f)
		for row in reader:
			firstname = row['firstname']
			lastname = row['lastname']
			email = row['email']
			promotion = row['promotion']
			names_list.append({'firstname':firstname, 'lastname': lastname})
			if email not in emails_list:
				emails_list.append(email)
			if promotion not in promotions_list:
				promotions_list.append(promotion)
	return render(request, 'customers/messaging.html',{'names': names_list, 'emails': emails_list, 'promotions':promotions_list})

@staff_member_required
def Download(request):
	filename = request.GET["filename"]
	with open(filename, 'rb') as myfile:
		response = HttpResponse(myfile, content_type='text/csv')
		response['Content-Disposition'] = 'attachment; filename=%s' %filename
	return response

@staff_member_required	
def Search(request):
	data = (json.loads(request.read().decode('utf-8')))
	filename = (data[0]['filename']).lower()
	search_term = (data[0]['search_term']).lower()
	search_list = search_term.split()

	contact_list = []
	with open(filename, 'r') as f:
		reader = csv.DictReader(f)
		for row in reader:
			name = row['fullname'].lower()
			email =  row['email'].lower()
			for each in search_list:
				if each in name or each in email:
					if row not in contact_list:
						contact_list.append(row)
	context = Context({'contacts': contact_list})
	html = get_template('customers/responsive-table.html').render(context)
	#print(html)
	return JsonResponse({'data': html})

@staff_member_required	
def Mail_Confirm(request):
	data = (json.loads(request.read().decode('utf-8')))
	contact_list = []
	promotion_codes_list = []
	message_contents = {}
	current_filename = 'storage/customers.csv'
	date_format = "%Y-%m-%d"
	send_to = data[0]['send_to']
	from_date = data[0]['from_date']
	to_date = data[0]['to_date']
	promotion_search = data[0]['promotion']
	code_given = data[0]['code_given']
	name_list = data[0]['name_list']
	email_list = data[0]['email_list']
	promotion_codes=data[0]['promotion_codes']
	subject = data[0]['subject'][0]
	message = data[0]['message'][0]
	#message_template = None
	message_contents['subject'] = subject

	if message:
		global message_template
		if message == 'Just ordered message - Seller review':
			message_template = 'customers/just_ordered.html'
		if message == 'Order recieved - Product review':
			message_template = 'customers/order_received.html'
		if message == 'Thank you - Coupon giveaway':
			message_template = 'customers/thankyou.html'
		if message == 'March Madness - Spring promo':
			message_template = 'customers/special_promotion.html'
	message_contents['message'] = message_template
	if email_list:
		with open(current_filename, 'r') as f:
			reader = csv.DictReader(f)
			for row in reader:
				found = 0
				email = row['email']
				for each in email_list:
					if each == email:
						for obj in contact_list:
							if email == obj['email']:
								found = 1
						if found == 0:
							contact_list.append(row)
	if name_list:
		#print('filtering name list')
		with open(current_filename, 'r') as f:
			reader = csv.DictReader(f)
			for row in reader:
				found = 0
				firstname = row['firstname']
				lastname = row['lastname']
				email = row['email']
				for each in name_list:
					if each == lastname + ', ' + firstname:
						for obj in contact_list:
							if email == obj['email']:
								found = 1
						if found == 0:
							contact_list.append(row)

	if promotion_codes:
		new_list = promotion_codes.split(',')
		for each in new_list:
			promotion_codes_list.append(each.strip(' '))
		with open(current_filename, 'r') as f:
			reader = csv.DictReader(f)
			for row in reader:
				promotion_code = row['promotion_code']
				for each in promotion_codes_list:
					if each == promotion_code:
						contact_list.append(row)
		
	
	if promotion_search:
		#print('filtering promotion_search')
		if code_given == "No Code Given":
			with open(current_filename, 'r') as f:
				reader = csv.DictReader(f)
				for row in reader:
					found = 0
					promotion = row['promotion']
					promotion_code = row['promotion_code']
					email = row['email']
					for each in promotion_search:
						if each == promotion and promotion_code == '':
							for obj in contact_list:
								if email == obj['email']:
									found = 1
							if found == 0:
								contact_list.append(row)
		if code_given == "Code Given":
			with open(current_filename, 'r') as f:
				reader = csv.DictReader(f)
				for row in reader:
					found = 0
					promotion = row['promotion']
					promotion_code = row['promotion_code']
					email = row['email']
					for each in promotion_search:
						if each == promotion and promotion_code !='':
							for obj in contact_list:
								if email == obj['email']:
									found = 1
							if found == 0:
								contact_list.append(row)
		elif code_given == "All":
			with open(current_filename, 'r') as f:
				reader = csv.DictReader(f)
				for row in reader:
					found = 0
					promotion = row['promotion']
					email = row['email']
					for each in promotion_search:
						if each == promotion:
							for obj in contact_list:
								if email == obj['email']:
									found = 1
							if found == 0:
								contact_list.append(row)
	if from_date != 'NaN-NaN-NaN' and to_date != 'NaN-NaN-NaN':
		date1 = datetime.datetime.strptime(from_date, date_format)
		date2 = datetime.datetime.strptime(to_date, date_format)
		with open(current_filename, 'r') as f:
			reader = csv.DictReader(f)
			for row in reader:
				found = 0
				date = datetime.datetime.strptime(row['date'][0:10], date_format)
				email = row['email']
				if date1 < date < date2:
					for obj in contact_list:
						if email == obj['email']:
							found = 1
					if found == 0:
						contact_list.append(row)		
	if send_to == 'All Contacts':
		with open(current_filename, 'r') as f:
			reader = csv.DictReader(f)
			for row in reader:
				found = 0
				email = row['email']
				for each in contact_list:
					if email == each['email']:
						found = 1
				if found == 0:
					contact_list.append(row)
	message_contents['contacts']= contact_list
	message_contents['message_storage_file'] = 'storage/message_storage_file.txt'
	with open('storage/message_storage_file.txt', 'wb') as f:
			pickle.dump(message_contents, f)
	#print(message_contents)
	context = Context({'message_contents': message_contents})
	html = get_template('customers/message_confirmation.html').render(context)
	htmlcontent = render_to_string(message_contents['message'], {'name': '<Customer name>'})
	#print(htmlcontent)
	return JsonResponse({'data': html, 'message': htmlcontent})

@staff_member_required	
def Send_Mail(request):
	data = (json.loads(request.read().decode('utf-8')))
	#print(data)
	filename = data[0]['filename']
	with open(filename, 'rb') as f:
			message_contents = pickle.load(f)
	subject = message_contents['subject']
	message = message_contents['message']
	for each in message_contents['contacts']:
		email = each['email']
		name = each['firstname']
		SendEmail.apply_async(args=[name, email, subject, message], countdown=5)
	return JsonResponse({'data': 'messages sent'})

@staff_member_required
def Mail_Sent(request):
	return render(request, 'customers/mail-sent.html')

@staff_member_required
def Files(request):
	files = os.listdir('storage/')
	return render(request, 'customers/files.html', {'files':files})

@staff_member_required
def Upload(request):
    if request.method == 'POST' and request.FILES['myfile']:
        myfile = request.FILES['myfile']
        fs = FileSystemStorage()
        fs.path('storage/')
        filename = fs.save(myfile.name, myfile)
        uploaded_file_name = myfile.name
        return render(request, 'customers/upload.html', {
            'uploaded_file_name': uploaded_file_name
        })
    return render(request, 'customers/upload.html')

@staff_member_required
def Add_Contacts(request):
	if request.is_ajax():
		data = (json.loads(request.read().decode('utf-8')))
		date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
		firstname = data[0]["firstname"]
		lastname = data[0]["lastname"]
		email = data[0]["email"]
		promotion = data[0]["promotion"]
		promotion_code = data[0]["promotion_code"]
		with open('storage/customers.csv', 'a') as csvfile:
			fieldnames = ['date', 'firstname', 'lastname', 'email', 'promotion', 'promotion_code']
			writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
			# writer.writeheader()
			writer.writerow({'date': date, 'firstname': firstname, 'lastname': lastname, 'email': email, 'promotion': promotion, 'promotion_code': promotion_code})
			return JsonResponse({'data': 'Contact saved successfuly.'})
	else:
		return render(request, 'customers/add.html')
